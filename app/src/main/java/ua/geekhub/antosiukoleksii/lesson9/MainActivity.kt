package ua.geekhub.antosiukoleksii.lesson9

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), UserListAdapter.AdapterCallback {

    private val newUserActivityRequestCode = 10;
    private lateinit var vm: GitVM
    private var lastAddedUserName: String? = null;

    override fun onItemClick(username: String) {
        intent = Intent(this@MainActivity, UserDetailsActivity::class.java)
        intent.putExtra(UserDetailsActivity.USERNAME_EXTRA, username)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = UserListAdapter(this)
        adapter.setCallback(this)
        usersList.adapter = adapter
        usersList.layoutManager = LinearLayoutManager(this)

        vm = GitVM(this.application, GitRepository.get(RoomDB.getDatabase(this), GitService()))
        vm.allSavedUsers.observe(this, Observer { users ->
            users?.let { adapter.setUsers(it) }
            if (lastAddedUserName != null) {
                onItemClick(this.lastAddedUserName!!)
            }
        })
    }

    fun onClickTest(view: View?) {
        val intent = Intent(this@MainActivity, NewUserActivity::class.java)
        startActivityForResult(intent, newUserActivityRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == newUserActivityRequestCode && resultCode == Activity.RESULT_OK) {
            data?.getStringExtra(NewUserActivity.EXTRA_REPLY)?.let {
                lastAddedUserName = it
                vm.addUserName(it)
            }
        } else {
            Toast.makeText(
                applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG).show()
        }
    }
}
