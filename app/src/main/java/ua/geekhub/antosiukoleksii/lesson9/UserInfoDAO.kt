package ua.geekhub.antosiukoleksii.lesson9

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserInfoDAO {

    @Query("SELECT * from userinfo_table order by name asc")
    fun getAlphabetizedUsers(): LiveData<List<UserInfo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(userinfo: UserInfo)

    @Query("select * from userinfo_table where login = :username")
    fun getUserByName(username: String): LiveData<UserInfo>
}