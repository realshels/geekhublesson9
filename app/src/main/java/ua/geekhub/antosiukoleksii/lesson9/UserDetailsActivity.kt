package ua.geekhub.antosiukoleksii.lesson9

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_user_details.*
import kotlinx.android.synthetic.main.userrecyclerlayout.*

class UserDetailsActivity : AppCompatActivity() {

    private fun setUserInfoToUI(user: UserInfo?) {
        textView10.text = user?.login ?: ""
        textView2.text = user?.created ?: ""
    }

    override fun onCreateView(
        parent: View?,
        name: String,
        context: Context,
        attrs: AttributeSet
    ): View? {
        if (this.intent.hasExtra(USERNAME_EXTRA)) {
            val username = this.intent.getStringExtra(USERNAME_EXTRA)
            GitRepository.get(RoomDB.getDatabase(this), GitService()).getUserByName(username).observe(this, Observer { item -> setUserInfoToUI(item)})
        }
        return super.onCreateView(parent, name, context, attrs)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)
    }

    companion object {
        val USERNAME_EXTRA = "USERNAME_EXTRA"
    }
}
