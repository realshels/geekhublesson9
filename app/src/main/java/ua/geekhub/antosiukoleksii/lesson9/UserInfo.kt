package ua.geekhub.antosiukoleksii.lesson9

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = "userinfo_table")
data class UserInfo(
    @PrimaryKey val id: Long,
    val login: String,
    val name: String?,
    val company: String?,
    val blog: String?,
    val location: String?,
    val email: String?,
    val followers: Int,
    val following: Int,
    @Json(name = "public_repos") val publicRepos: Int,
    @Json(name = "avatar_url") val avatarUrl: String,
    @Json(name = "html_url") val url: String,
    @Json(name = "created_at") val created: String,
    @Json(name = "updated_at") val updated: String
)