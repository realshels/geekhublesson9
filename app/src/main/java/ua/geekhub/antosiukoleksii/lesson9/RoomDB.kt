package ua.geekhub.antosiukoleksii.lesson9

import android.content.Context
import androidx.lifecycle.ViewModelStore
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(UserInfo::class), version = 1, exportSchema = false)
public abstract class RoomDB : RoomDatabase() {
    abstract fun userinfoDAO(): UserInfoDAO;

    companion object {
        @Volatile
        private var INSTANCE: RoomDB? = null

        fun getDatabase(context: Context): RoomDB {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RoomDB::class.java,
                    "gitDB"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}