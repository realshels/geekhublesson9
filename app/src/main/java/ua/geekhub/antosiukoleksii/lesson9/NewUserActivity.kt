package ua.geekhub.antosiukoleksii.lesson9

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import kotlinx.android.synthetic.main.activity_new_user.*

class NewUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)
    }

    fun onClickSave(view: View?) {
        val replyIntent = Intent()
        if (TextUtils.isEmpty(edit_user.text)) {
            setResult(Activity.RESULT_CANCELED, replyIntent)
        } else {
            val word = edit_user.text.toString()
            replyIntent.putExtra(EXTRA_REPLY, word)
            setResult(Activity.RESULT_OK, replyIntent)
        }
        finish()
    }

    companion object {
        const val EXTRA_REPLY = "NewUserREPLY"
    }
}
