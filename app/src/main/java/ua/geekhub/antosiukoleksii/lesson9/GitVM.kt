package ua.geekhub.antosiukoleksii.lesson9

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class GitVM(application: Application,
            private val repos: GitRepository): AndroidViewModel(application) {

    val allSavedUsers: LiveData<List<UserInfo>> = repos.allSavedUsers

    fun addUserName(username: String) {
        viewModelScope.launch {
            val res =  repos.getUserInfo(username)
//            if (res is ResultStatus.Success) {
//                res.data
//            }
        }
    }
}
