package ua.geekhub.antosiukoleksii.lesson9

import androidx.lifecycle.LiveData

class GitRepository private constructor(
    private val db: RoomDB,
    private val service: GitService
//    private val netManager: NetManager
) {

//    private val dao by lazy { db.gitUserReposDao() }

//    private val isNetworkAvailable get() = netManager.isNetworkAvailable

    val allSavedUsers: LiveData<List<UserInfo>> = db.userinfoDAO().getAlphabetizedUsers()

    fun getUserByName(username: String): LiveData<UserInfo> = db.userinfoDAO().getUserByName(username)

    suspend fun getUserInfo(userName: String?): ResultStatus<UserInfo> {
//        val user = userName?.let { findUserInDb(it) }
//        return when {
//            userName == null -> {
//                if (isNetworkAvailable){
//                    Status.Success(getAllSavedUsers())
//                } else {
//                    Status.DbSuccess(getAllSavedUsers())
//                }
//            }
//            user != null -> {
//                if (isNetworkAvailable){
//                    Status.Success(listOf(user))
//                } else {
//                    Status.DbSuccess(listOf(user))
//                }
//            }
//            isNetworkAvailable ->
            if (userName != null) {
                return doGetUserInfo(userName)
            }
        return ResultStatus.Error(Exception())
//            else ->                 Status.NetworkFailed(getAllSavedUsers())
//        }
    }

//    suspend fun removeUserFromDb(login: String) =
//        dao.delete(login)

//    fun closeDb() = db.close()

    private suspend fun doGetUserInfo(userName: String): ResultStatus<UserInfo> {
        try {
            val user = getUserInfoFromAPI(userName)
            db.userinfoDAO().insert(user)
//            val repos = gitReposByUsername(userName)

            return ResultStatus.Success(user)

        } catch (e: Exception) {
            return ResultStatus.Error(e)
        }
    }

    private suspend fun getUserInfoFromAPI(userName: String) =
        service.getUserInfo(userName)

//    private suspend fun gitReposByUsername(userName: String) =
//        service.gitReposByUsername(userName)

//    private suspend fun getAllSavedUsers() =
//        dao.getAllSavedUsers()

//    private suspend fun putUserToDb(user: GitUserWithRepos) =
//        dao.insert(user)

//    private suspend fun findUserInDb(login: String) =
//        dao.findByLogin(login)

    companion object {

        @Volatile
        private var INSTANCE: GitRepository? = null

        fun get(db: RoomDB, service: GitService
//                , netManager: NetManager
        ) =
            INSTANCE
                ?: synchronized(this) {
                    INSTANCE
                        ?: GitRepository(
                            db,
                            service
//                            netManager
                        ).also {
                            INSTANCE = it
                        }
                }
    }
}