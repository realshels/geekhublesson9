package ua.geekhub.antosiukoleksii.lesson9

import retrofit2.http.GET
import retrofit2.http.Path

interface ApiClient {
    @GET("users/{username}")
    suspend fun getUserInfo(@Path("username") username: String): UserInfo
}