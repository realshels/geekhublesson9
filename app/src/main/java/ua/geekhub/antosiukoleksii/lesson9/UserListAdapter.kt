package ua.geekhub.antosiukoleksii.lesson9

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UserListAdapter internal constructor(context: Context) : RecyclerView.Adapter<UserListAdapter.UsersViewHolder>() {

    interface AdapterCallback {
        fun onItemClick(username: String)
    }

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var users = emptyList<UserInfo>()
    private var callback: AdapterCallback? = null

    fun setCallback(callback: AdapterCallback) {
        this.callback = callback
    }

    inner class UsersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            callback?.onItemClick(this.userItemView.text.toString())
        }

        val userItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val itemView = inflater.inflate(R.layout.userrecyclerlayout, parent, false)
        return UsersViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        val current = users[position]
        holder.userItemView.text = current.login
    }

    internal fun setUsers(words: List<UserInfo>) {
        this.users = words
        notifyDataSetChanged()
    }

    override fun getItemCount() = users.size
}