package ua.geekhub.antosiukoleksii.lesson9

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Protocol
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class GitService {

    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val okHttpClient = OkHttpClient.Builder()
        .protocols(listOf(Protocol.HTTP_1_1))
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    private val client = retrofit.create(ApiClient::class.java)

    suspend fun getUserInfo(userName:String): UserInfo {
        return client.getUserInfo(userName)
    }

//    suspend fun gitReposByUsername(userName: String) = withContext(Dispatchers.IO){
//        client.fetchGitReposByUsername(userName)
//    }

}