package ua.geekhub.antosiukoleksii.lesson9

sealed class ResultStatus<out T: Any> {
    data class Success<out T : Any>(val data: T) : ResultStatus<T>()
    data class Error(val exception: Exception) : ResultStatus<Nothing>()
}